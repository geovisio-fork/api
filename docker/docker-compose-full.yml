# complete docker compose with geovisio backend, a database, a blurring API and a keycloak for authentication
# they use the local network in order for the oauth dance to work (keycloak should be accessible by the `backend` service and the user's browser)
# * the keycloak is accessible through http://localhost:8182
# * the backend is accessible through http://localhost:5000
# * the blurring API is accessible through http://localhost:5500
# * the website is accessible through http://localhost:3000
# (the database is not on the local network since only the backend need to access it)
# if the port are alreay binded on your system, change them in this file

services:
  auth:
    command: start-dev --import-realm
    environment:
      GEOVISIO_BASE_URL: http://localhost:5000
      GEOVISIO_CLIENT_SECRET: what_a_secret
      KEYCLOAK_ADMIN: admin
      KEYCLOAK_ADMIN_PASSWORD: password
      KEYCLOAK_FRONTEND_URL: http://localhost:5000/api/auth/login
      KC_HTTP_PORT: 8182
    healthcheck:
      test: curl --fail http://localhost:8182/realms/geovisio
      timeout: 5s
      interval: 5s
      retries: 5
      start_period: 60s
    image: quay.io/keycloak/keycloak:20.0.1
    ports:
      - 8182:8182
    volumes:
      - ./keycloak-realm.json:/opt/keycloak/data/import/geovisio_realm.json

  migrations:
    image: geovisio/api:latest
    build:
      context: ..
      dockerfile: Dockerfile
      cache_from:
        - registry.gitlab.com/geovisio/api:build_cache
    command: db-upgrade
    environment:
      DB_URL: postgres://gvs:gvspwd@db/geovisio
    depends_on:
      db:
        condition: service_healthy

  api:
    image: geovisio/api:latest
    build:
      context: ..
      dockerfile: Dockerfile
      cache_from:
        - registry.gitlab.com/geovisio/api:build_cache
    command: api
    depends_on:
      auth:
        condition: service_healthy
      db:
        condition: service_healthy
      migrations:
        condition: service_completed_successfully
    environment:
      OAUTH_CLIENT_ID: geovisio
      OAUTH_CLIENT_SECRET: what_a_secret
      DB_URL: postgres://gvs:gvspwd@db/geovisio
      OAUTH_OIDC_URL: http://localhost:8182/realms/geovisio
      OAUTH_PROVIDER: oidc
      FLASK_SECRET_KEY: a_very_secret_key_never_to_be_used_in_production
      FLASK_SESSION_COOKIE_HTTPONLY: false # the website needs to be able to decode flask's session cookie
      API_FORCE_AUTH_ON_UPLOAD: True
      PICTURE_PROCESS_THREADS_LIMIT: 0
      PICTURE_PROCESS_DERIVATES_STRATEGY: PREPROCESS
      FS_URL: /data/geovisio
      NB_API_THREADS: 10
    ports:
      - 5000:5000
    volumes:
      - pic_data:/data/geovisio
    extra_hosts:
      - "localhost:host-gateway"

  website:
    image: geovisio/website:latest
    environment:
      VITE_INSTANCE_NAME: GeoVisio Docker
      VITE_API_URL: http://localhost:5000/
    ports:
      - 3000:3000

  # Background workers used to process pictures in the background
  # calling the blur API and generating derivates (SD picture and tiles for faster rendering in photosphereviewer)
  # Several background workers can run together
  # Note: the blurring is done only if API_BLUR_URL is configured, and it's not the case in this docker compose
  # To have blurring, the docker-compose-blurring.yml file needs to be added (cf documentation in this docker compose file)
  background-worker:
    image: geovisio/api:latest
    build:
      context: ..
      dockerfile: Dockerfile
      cache_from:
        - registry.gitlab.com/geovisio/api:build_cache
    command: picture-worker
    depends_on:
      db:
        condition: service_healthy
      migrations:
        condition: service_completed_successfully
    restart: always
    environment:
      DB_URL: postgres://gvs:gvspwd@db/geovisio
      PICTURE_PROCESS_DERIVATES_STRATEGY: PREPROCESS
      FS_URL: /data/geovisio
    deploy:
      mode: replicated
      replicas: 5 # by default this number of workers will be run. This can be change at runtime with `docker compose up background-worker -d --scale background-worker=<VALUE>`
    volumes:
      - pic_data:/data/geovisio
    extra_hosts:
      - "localhost:host-gateway"

  db:
    environment:
      POSTGRES_DB: geovisio
      POSTGRES_PASSWORD: gvspwd
      POSTGRES_USER: gvs
    healthcheck:
      # double check to detect the fact that PG starts twice on startup
      test: pg_isready -q -d $$POSTGRES_DB -U $$POSTGRES_USER && sleep 1 && pg_isready -q -d $$POSTGRES_DB -U $$POSTGRES_USER
      timeout: 5s
      interval: 5s
      retries: 5
      start_period: 60s
    image: postgis/postgis:13-3.2
    ports:
      - 5445:5432
    volumes:
      - postgres_data:/var/lib/postgresql/data/

volumes:
  postgres_data:
    name: geovisio_postgres_data
  pic_data: {}
