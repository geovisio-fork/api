-- tokens
-- depends: 20230427_01_k5e5w-timestamps


DROP TABLE IF EXISTS tokens;

DROP FUNCTION IF EXISTS generate_default_token CASCADE;