# This contains data needed by the migrations

The file sensor_data.json is from https://github.com/mapillary/OpenSfM/raw/main/opensfm/data/sensor_data.json

The file can be updated if needed, but it will then only be available for instances that has not yet applied the migration.
