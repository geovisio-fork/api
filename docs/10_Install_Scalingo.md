# Install GeoVisio on Scalingo

The GeoVisio API can be easily deployed on [Scalingo](https://scalingo.com/) solutions. All [necessary settings](./11_Server_settings.md) should be defined as environment variables. Various fixtures were defined in GeoVisio to make the process as straightforward as possible.

General documentation for deploying applications on Scalingo is [available on their website](https://doc.scalingo.com/platform/deployment/deploy-with-git).

## Next step

You can read more about [server settings](./11_Server_settings.md).
