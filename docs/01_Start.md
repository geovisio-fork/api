# GeoVisio hands-on guide

![GeoVisio logo](../images/logo_full.png)

Welcome to GeoVisio __API__ documentation ! It will help you through all phases of setup, run and develop on GeoVisio API and backend.

__Note that__ this only covers the API component, if you're looking for docs on another component, you may go to [this repository](https://gitlab.com/geovisio) instead.

Also, if at some point you're lost or need help, you can contact us through [issues](https://gitlab.com/geovisio/api/-/issues) or by [email](mailto:panieravide@riseup.net).

You might want to dive into docs :

- Install an instance
  - Using a classic install
    - [Setup your database](./07_Database_setup.md)
    - [Install Geovisio and dependencies](./10_Install_Classic.md)
  - [Using Scalingo solutions](./10_Install_Scalingo.md)
  - [Using Docker](./14_Running_Docker.md)
- [Available settings](./11_Server_settings.md)
- [Enable authentication with external identity providers](./12_External_Identity_Providers.md)
- Start and use your server
  - [Classic-deployed](./14_Running_Classic.md)
  - [Docker-deployed](./14_Running_Docker.md)
- [Organize your pictures and sequences](./15_Pictures_requirements.md)
- [Work with HTTP API](./16_Using_API.md)
- Advanced server topics
  - [Blurring API](./17_Blur_API.md)
  - [Developing on the server](./19_Develop_server.md)
  - [Differences with standard STAC API](./80_STAC_Compatibility.md)
